output "load_balancer_address" {
  value = "http://${aws_lb.main.dns_name}"
}