# ecs-demo

Demonstrating ECS cluster builds via Terraform.
Features:

- Builds an ECS cluster on Fargate
    - Shows IP, date/time, etc.
    - Scalable: default of 2, minimum of 1, maximum of 5
    - Minimal network access
    - Fault tolerant/highly available across three availability zones
    - ✨ My first ECS cluster ✨
- Builds encrypted bucket for potential application data
- Using Terraform
    - Uses modules and resources
    - Split across files for grouping of elements
    - Follows industry best practices
    - Could be variablized till the cows come home
- Pipeline
    - GitLab CI/CD with Terraform Cloud backend
    - All Terraform stages, with approval gate before destroy
    - Room to grow with SDLC, unit testing, linting, SAST, etc.