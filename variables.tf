variable "app_count" {
  type    = number
  default = 2
}

variable "app_name" {
  type    = string
  default = "ecs-demo"
}