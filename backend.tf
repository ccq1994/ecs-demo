terraform {
  cloud {
    organization = "ccq1994"

    workspaces {
      name = "ecs-demo"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}